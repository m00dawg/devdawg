   /* DevDawg Rotary M0
 *  
 * Open Source Arduino-Based Rotary Film Controller.
 * by Tim Soderstrom
 *  
 *  Licensed under the GPLv3 (see LICENSE for more information)
 *  
 *  This is a smaller program that just handles turning the rotary
 *  (so no display output, no temps, etc.)
 *
 *  This is the Adafruit Trinket M0 Version (ARM)
 */

/**********
 * Defines 
 **********/
#define SECONDS_MS 1000
#define SECONDS_PER_MINUTE 60

#define FORWARD 1
#define REVERSE -1

#define ON 1
#define OFF 0

/*******
 * Pins
 *******/
#define MOTOR_1A 0
#define MOTOR_2A 1
#define MOTOR_3A 2
#define MOTOR_4A 3
#define BUTTON 4

/**********
 * Dotstar
 **********/
#define NUMPIXELS  1
#define DATAPIN    7
#define CLOCKPIN   8
#define POWER_LED 13

/***********
 * Includes 
 ***********/
#include <Stepper.h>
#include <Adafruit_DotStar.h> // This is just here to turn off the damn on board LED

/***********
 * Constants
 ***********/
const int stepsPerRevolution = 200;               // The steps per revolution of the stepper
const int rpm = 45;                               // The target RPM
const int directionInvervalMS = 10 * SECONDS_MS;  // How many milliseconds before we change direction
const int directionDelayMS = 100;                 // How many milliseconds to pause for on direction change
const int speedRampAdd = 3;                       // How many steps to add to the speed when we are ramping up
const int buttonDebounceDelayMS = 250;            // How many milliseconds to pause for debounce (this is blocking)

/************
 * Variables 
 ************/
unsigned long directionMillis;
unsigned long speedMillis;
bool motorState;
int motorDirection;
int currentSpeed;
int stepsPerSecond;

/*********
 * Objects
 *********/
Stepper stepper(stepsPerRevolution, MOTOR_1A, MOTOR_2A, MOTOR_3A, MOTOR_4A);
Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);


/*******
 * Code 
 ********/

void setup() {
  // Turn off that incredibly bright onboard LED
  pinMode(POWER_LED, OUTPUT);
  digitalWrite(POWER_LED, LOW); 

  // Turn off the other incredibly bright LED
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  
  // Initialize the Button
  pinMode(BUTTON, INPUT_PULLUP);
}

void loop() {
  /* Check for button press */
  if(digitalRead(BUTTON) == LOW) {
    delay(buttonDebounceDelayMS);
    if(motorState == ON) {
      motorState = OFF;
    }
    else {
      motorState = ON;
      /* Reset the direction value to prevent rotating less than the dsired interval */
      directionMillis = millis();
    } 
  }

  /* Motor is running */
  if(motorState == ON)
  {
    /* Change direction every interval seconds, pausing for a bit in between */
    if(directionMillis + directionInvervalMS <= millis()) {
      stepper.setSpeed(0);
      delay(directionDelayMS);
      currentSpeed = 0;
      if(motorDirection == FORWARD) {      
        motorDirection = REVERSE;
      }
      else {
        motorDirection = FORWARD;
      }
      directionMillis = millis();
    }
  
    if(currentSpeed < rpm && speedMillis <= millis())
    {
      currentSpeed += speedRampAdd;
      stepper.setSpeed(currentSpeed);
      speedMillis = millis();
    }
  
    /* Run the motor */
    if(motorDirection == FORWARD) {
      stepper.step(1);
      //stepper.setSpeed(currentSpeed);    
    }
    else {
      stepper.step(-1);
      //stepper.setSpeed(currentSpeed * -1);
    }
  }
  /* Motor is disabled */
  else {
    currentSpeed = 0;
    stepper.setSpeed(0);
  }
}
