void updateDisplay(int totalSeconds, int temperature)
{
  char buffer[32];
  sprintf(buffer, "%02d:%02d / %02dC", int(totalSeconds / 60), int(totalSeconds % 60), temperature);
  lcd.setCursor(0,1);
  lcd.print(buffer);
}

byte wait(char line1[16], char line2[16])
{
  byte button=0;
  drawDisplay(line1, line2);
  while(!button)
  {
    button = lcd.readButtons();
    controlTemp();
  }
  lcd.clear();
  delay(250);
  return button;
}

void drawDisplay(char line1[16], char line2[16])
{
  lcd.clear();
  lcd.print(line1);
  lcd.setCursor(0,1);
  lcd.print(line2);
}

double collectTemperatures()
{
  sensors.requestTemperatures();
  if(sensors.getAddress(thermometer, 0))
  {
    return double(sensors.getTempC(thermometer));
  }
  return -255;
}


void controlTemp()
{
  pid.Compute();  
  if (millis() - pidWindowStartTime > PID_WINDOW_SIZE_MILLIS)
    pidWindowStartTime += PID_WINDOW_SIZE_MILLIS;
  if (pidOutput > millis() - pidWindowStartTime)
  {    
    if(heaterState != ON)
    {
      heaterState = ON;
      //Serial.println("Turned Heater On");
    }
    digitalWrite(heater, HIGH);
  }
  else
  {
    if(heaterState != OFF)
    {
      heaterState = OFF;
      //Serial.println("Turned Heater Off");
    }
    digitalWrite(heater, LOW);
  }
}

void controlMotor(byte speed, byte directionInterval)
{
  if(directionInterval > 0)
  {
    // Coast on direction change
    if(motorCoast)
    {
      analogWrite(motorSpeed, 0);
      motorCoasting = true;
      if (millis() - previousCoastMillis >= COAST_MOTOR_MILLIS && motorCoast)
      {
        Serial.println("Done Coasting");
        motorCoasting = false;
      }
    }
    // If we're not coasting, make sure we set the speed to whatever is being requested
    else if (!motorCoast)
      analogWrite(motorSpeed, toSpeed(speed));
    if(millis() - previousMotorMillis >= SECONDS_MS * directionInterval)
    { 
      // Flag we need to coast the motor
      // If we are coasting, do nothing
      if(motorCoasting)
      {
        
      }
      // If we are not currently coasting, but 
      else if(!motorCoast)
      {
        Serial.println("Coast Requested");
        previousCoastMillis = millis();
        motorCoast = true;
      }
      // Now Change Direction
      else if(!motorCoasting)
      {
        Serial.println("Motor Running");
        analogWrite(motorSpeed, toSpeed(speed));
        changeDirection();
        motorCoast = false;
        previousMotorMillis = millis();
      }
    }
  }
  /* If directionInterval is zero, never change speed */
  else
  {
    analogWrite(motorSpeed, toSpeed(speed));
    digitalWrite(motorX, HIGH);
    digitalWrite(motorY, LOW);
    motorDirection = FORWARD;
  }
}

void changeDirection()
{
  if(motorDirection == FORWARD)
  {
    Serial.println("Motor: Reverse");
    digitalWrite(motorX, LOW);
    digitalWrite(motorY, HIGH);
    motorDirection = REVERSE;
  }
  else
  {
    Serial.println("Motor: Forward");
    digitalWrite(motorX, HIGH);
    digitalWrite(motorY, LOW);
    motorDirection = FORWARD;
  }
}

/*
int toRPM(byte value)
{
  return map(value, 0, 255, 0, MOTOR_RPM);
}
*/

int toSpeed(byte value)
{
  // return map(value, 0, MOTOR_RPM, 0, 255);
  // Changing to percent over raw RPMs
  return map(value, 0, 100, 0, 255);
}
