 /* DevDawg
 *  
 * Open Source Arduino-Based Rotary Film Development Controller.
 * by Tim Soderstrom
 *  
 *  Licensed under the GPLv3 (see LICENSE for more information)
 */

/**********
 * Defines 
 **********/
#define VERSION "v0.10"
#define BANNER_DELAY_MS 1000

#define RED 0x1
#define YELLOW 0x3
#define GREEN 0x2
#define TEAL 0x6
#define BLUE 0x4
#define VIOLET 0x5
#define WHITE 0x7

#define LCD_COLUMNS 16
#define LCD_ROWS 2

#define SECONDS_MS 1000

#define FORWARD 0
#define REVERSE 1

#define ON 1
#define OFF 0

// Menu Options
#define MAIN_MENU 0
#define DEVELOP_FILM 1
#define PREHEAT 2
#define RUN_MOTOR 3
#define HEAT_AND_MOTOR 4
#define MAX_OPTION 4

// Tunable Defines
// Maximum number of dev steps a particular Dev Process can have.
#define MAX_DEV_STEPS 8
// How often to check the temperature probe
#define TEMP_UPDATE_INTERVAL 5
// Default Preheat Temperature (C)
#define DEFAULT_PREHEAT_TEMP 39
// Manual Temperature Adjustment Steps
#define TEMP_ADJUSTMENT_PRECISION 0.1
// Specified Motor RPM
// #define MOTOR_RPM 76
// Motor Start Percentage
#define MOTOR_START_PCT 100
// Slowest Motor Will Turn
// #define MOTOR_MIN_RPM 24
#define MOTOR_MIN_PCT 50
// How long to free spin motor when changing direction
// #define COAST_MOTOR_MILLIS 500
#define COAST_MOTOR_MILLIS 750

// How many seconds between direction changes
#define DEFAULT_DIRECTION_INTERVAL 10

// PID Window Size
#define PID_WINDOW_SIZE_MILLIS 5000
#define PID_SAMPLE_TIME 1000

// PID Parameters
//#define KP 2
//#define KI 5
//#define KD 1
#define KP 4
#define KI 5
#define KD 2


/***********
 * Includes 
 ***********/
// Maybe for the future for storing and easily transferring development processes
// Might be simpler to use I2C and an EEPROM but then there needs to be a mechanicsm
// to add/remove development options
// #include <SPI.h>
// #include <SD.h>
#include <PID_v1.h>
#include <Wire.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Adafruit_RGBLCDShield.h>
#include "structs.h"

/*******
 * Pins
 *******/
const byte temperatureProbes = 2;
const byte buzzer = 3;
const byte heater = 4;
const byte motorSpeed = 5;
const byte motorX = 6;
const byte motorY = 7;
const byte recircPump = 8;

/************
 * Variables 
 ************/
byte pickedProcess = 0;
byte button = 0;
byte mode = 0;
byte option = 0;
double pidOutput;
double currentTemperature = 0;
double targetTemperature = DEFAULT_PREHEAT_TEMP;
unsigned long pidWindowStartTime;
//unsigned int pidWindowSizeMS = 5000;
bool heaterState = OFF;
unsigned long previousMotorMillis = 0;
unsigned long previousCoastMillis = 0;
int motorSpeedCurrent = 0;
bool motorDirection = FORWARD;
// If we need to coast
bool motorCoast = true;
// If we are actively coasting
bool motorCoasting = true;

/**********
 * Objects 
 **********/
Adafruit_RGBLCDShield lcd = Adafruit_RGBLCDShield();

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(temperatureProbes);
DallasTemperature sensors(&oneWire);
DeviceAddress thermometer;

// PID Object
PID pid(&currentTemperature, &pidOutput, &targetTemperature, KP, KI, KD, DIRECT);

/******************************
 * Development Process Structs 
 ******************************/

// Dev Processes (These can eventually be in the EEPROM)
const byte processCount = 2;
// Dev Processes
const DevProcess process[] =
{
  {
    "ECN-2",
    75,
    10,
    41.1,
    5,
    {
      {"Develop", 180, RED},
      {"Stop", 60, YELLOW},
      {"Bleach", 180, VIOLET},
      {"Fix", 180, WHITE},
      {"Rinse", 60, WHITE}
    }
  },
  {
    "Test",
    75,
    10,
    41.1,
    5,
    {
      {"Develop", 600, RED},
      {"Stop", 60, YELLOW},
      {"Bleach", 180, VIOLET},
      {"Fix", 180, WHITE},
      {"Rinse", 60, WHITE}
    }
  }
};

DevProcess currentProcess;

/*******
 * Code 
 ********/

void setup()
{
  pinMode(buzzer, OUTPUT);
  pinMode(heater, OUTPUT);
  pinMode(motorSpeed, OUTPUT);
  pinMode(motorX, OUTPUT);
  pinMode(motorY, OUTPUT);
  pinMode(recircPump, OUTPUT);
  
  digitalWrite(buzzer, LOW);
  digitalWrite(heater, LOW);
  analogWrite(motorSpeed, 0);
  digitalWrite(motorX, LOW);
  digitalWrite(motorY, LOW);
  digitalWrite(recircPump, LOW);
  
  Serial.begin(9600); 
  lcd.begin(LCD_COLUMNS, LCD_ROWS);

  /* Print Title and Version */
  lcd.setBacklight(YELLOW);
  lcd.setCursor(0,0);
  lcd.print(F("DevDawg"));
  lcd.setCursor(0,1);
  lcd.print(VERSION);
  delay(BANNER_DELAY_MS);

  /* Setup PID */
  pidWindowStartTime = millis();
  pid.SetOutputLimits(0, PID_WINDOW_SIZE_MILLIS);
  pid.SetMode(AUTOMATIC);

  currentTemperature = collectTemperatures();
}

void loop()
{
  /* Menu Functions */
  switch(mode)
  {
    case PREHEAT:
    {
      preheat();
      mode = 0;
      break;
    }
    case DEVELOP_FILM:
    {
      developFilm();
      mode = 0;
      break;
    }
    case RUN_MOTOR:
    {
      runMotor();
      mode = 0;
      break;
    }
    case HEAT_AND_MOTOR:
    {
      heatAndMotor();
      mode = 0;
      break;
    }
  
    // Main Menu
    default:
    {
      switch(option)
      {
        case DEVELOP_FILM:
        {
          button = wait("Select Mode", "Develop Film");
          break;
        }
        case PREHEAT:
        {
          button = wait("Select Mode", "Preheat");
          break;
        }
        case RUN_MOTOR:
        {
          button = wait("Select Mode", "Turn Rotary");
          break;
        }
        case HEAT_AND_MOTOR:
        {
          button = wait("Select Mode", "Heat & Motor");
          break;
        }
        default:
        {
          button = wait("Main Menu","");
          break;
        }
      }
      delay(250);
      switch(button)
      {
        case BUTTON_UP:
        {
          ++option;
          if(option > MAX_OPTION)
            option = 1;
          Serial.println(F("Button Up"));
          Serial.print(F("Option: "));
          Serial.println(option);
          break;
        }
        case BUTTON_DOWN:
        {
          --option;
          if(option < 0 || option > MAX_OPTION)
            option = MAX_OPTION;
          Serial.println(F("Button Down"));
          Serial.print(F("Option: "));
          Serial.println(option);
          break;
        }
        case BUTTON_SELECT:
        {
          Serial.println(F("Button Select"));
          mode = option;
          option = 0;
          break;
        }
      }
    }
  }

  // Reset back to main menu
  button = 0;
}
