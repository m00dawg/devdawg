

// Pick a development process using the LCD/Buttons 
byte pickProcess()
{
  // Selected process (by array index)
  byte processIndex = 0;
  byte button = 0;

  while(true)
  {
    while(!button)
      button = wait("Dev for", process[processIndex].name);
    if (button & BUTTON_UP)
    {
      ++processIndex;
      if(processIndex > processCount - 1)
        processIndex = 0;
    }
    if (button & BUTTON_DOWN)
      --processIndex;
      // Greater than because we are using a byte so we need to check
      // for overflow rather than negative numbers.
      if(processIndex > processCount - 1)
        processIndex = processCount - 1;
    if (button & BUTTON_SELECT)
      return processIndex;
    button = 0;
    delay(250);
    Serial.print("Index: ");
    Serial.println(processIndex);
  }
}

// Run a development step
void processStep(DevStep step, byte motorSpeed, byte motorDirectionInterval, int targetTemperature)
{
  unsigned long currentMillis = millis();
  unsigned long previousMillis = 0;
  long timeRemainingMS = long(step.duration) * SECONDS_MS;
  int currentTemperature = collectTemperatures();
  byte tempUpdateCycle = 0;
  byte motorUpdateCycle = 0;
  byte motorDirection = FORWARD;
  
  Serial.println(step.duration);
  Serial.println(motorSpeed);
  Serial.println(targetTemperature);
  Serial.println(currentTemperature);
  Serial.println(timeRemainingMS);

  lcd.setBacklight(step.color);
  lcd.print(step.name);

  previousMillis = millis();
  while(timeRemainingMS >= 0)
  {
    currentMillis = millis();
    controlTemp();
    controlMotor(motorSpeed, motorDirectionInterval);
    // Update once a second
    if(currentMillis - previousMillis >= SECONDS_MS)
    {
      Serial.print("Time remaining: ");
      Serial.println(timeRemainingMS);
      
      // Every TEMP_UPDATE_INTERVAL seconds, check temperature
      if(tempUpdateCycle > TEMP_UPDATE_INTERVAL - 1)
      {
        currentTemperature = collectTemperatures();
        tempUpdateCycle = 0;
        //Serial.print("Temperature: ");
        //Serial.println(currentTemperature);
      }
      else
        ++tempUpdateCycle;
      updateDisplay(int(timeRemainingMS / SECONDS_MS), currentTemperature);
      timeRemainingMS -= SECONDS_MS;
      /* This is clever - it takes time to do all the stuff above and this could
       *  cause drifting. So instead we update the previousMillis by 1000 (1 second)
       *  See: https://forum.arduino.cc/index.php?topic=587214.0
       *  Of note, the Timer library FTW?
       */
      previousMillis += SECONDS_MS;
      //previousMillis = millis();
    }
  }
}

// Run a full development process
void developFilm()
{
  pickedProcess = pickProcess();
  currentProcess = process[pickedProcess];
  wait(currentProcess.name, "Press Any Key");
  controlMotor(0, 0);
  // After picking a process, pre-heat then start dev
  targetTemperature = currentProcess.targetTemperature;
  //preheat();
  for(byte i = 0 ; i < currentProcess.steps; ++i)
  {
    processStep(currentProcess.devStep[i], 
      currentProcess.motorSpeed, 
      currentProcess.motorDirectionInterval, 
      currentProcess.targetTemperature);
    //Serial.println("Motor: Stopping");
    controlMotor(0, 0);
    //Serial.println("done");
    wait(currentProcess.devStep[i].name, "Complete");
  }
  lcd.setBacklight(GREEN);
  wait("Dev Cycle", "Complete");
}

void preheat()
{
  unsigned long previousMillis = 0;
  byte button = 0;
  char buffer[17];
  char currentTempChar[6];
  char targetTempChar[6];
  
  while(true)
  {
    controlTemp();
    if(millis() - previousMillis >= SECONDS_MS * TEMP_UPDATE_INTERVAL || button)
    {
      currentTemperature = collectTemperatures();
      Serial.print("Current Temperature: ");
      Serial.println(currentTemperature);
      Serial.print("Target Temperature: ");
      Serial.println(targetTemperature);
      Serial.print("PID Output: ");
      Serial.println(pidOutput);
      /* Removing for now since for mixing chemicals we want it to still adjust
       *  temperatures (since heating the chemicals would bring the water
       *  temperature down over time).
      if(currentTemperature == targetTemperature)
      {
        wait("Target", "Reached");
        return;
      }
      else
      {
      */
      dtostrf(currentTemperature, 5, 2, currentTempChar);
      dtostrf(targetTemperature, 5, 2, targetTempChar);
      sprintf(buffer, "%sC / %sC", currentTempChar, targetTempChar);
      //sprintf(buffer, "%02dC / %02dC", currentTemperature, targetTemperature);
      drawDisplay("Preheating", buffer);
      previousMillis = millis();
    }
    button = lcd.readButtons();
    if(button & BUTTON_LEFT)
      return;
    if(button & BUTTON_UP)
      targetTemperature += TEMP_ADJUSTMENT_PRECISION;
    if(button & BUTTON_DOWN)
    targetTemperature -= TEMP_ADJUSTMENT_PRECISION;
  }
}

void runMotor()
{
  // byte rpm = MOTOR_RPM;
  byte rpm = MOTOR_START_PCT;
  byte directionInterval = DEFAULT_DIRECTION_INTERVAL;
  bool runMotor = true;
  char buffer[32];
  byte button=0;

  sprintf(buffer, "%02d%% : I %02d", rpm, directionInterval);
  drawDisplay("Turning Rotary", buffer); 
  while(true)
  {
    if(runMotor)
      controlMotor(rpm, directionInterval);
    else
      controlMotor(0, 0);
    button = lcd.readButtons();
    if(button)
    {
      Serial.println("In button if");
      switch(button)
      {
        case BUTTON_UP:
        {
          //if(rpm < MOTOR_RPM)
          if(rpm < 100)
            ++rpm;
          button = 0;
          break;
        }
        case BUTTON_DOWN:
        {
          //if(rpm > MOTOR_MIN_RPM)
          if(rpm > MOTOR_MIN_PCT)
            --rpm;
          button = 0;
          break;
        }
        case BUTTON_LEFT:
        {
          if(directionInterval > 1)
            --directionInterval;
          button = 0;
          break;
        }
        case BUTTON_RIGHT:
        {
          if(directionInterval < 255)
            ++directionInterval;
          button = 0;
          break;
        }
        case BUTTON_SELECT:
        {
          if(runMotor)
          {
            drawDisplay("Turning Rotary", "Paused"); 
            runMotor = false;
          }
          else
            runMotor = true;
        }
      }
      if(runMotor)
      {
        sprintf(buffer, "%02d%% : I %02d", rpm, directionInterval);
        drawDisplay("Running Motor", buffer); 
      }
    }
  }
}

void heatAndMotor()
{
  unsigned long previousMillis = 0;
  byte button = 0;
  char currentTempChar[6];
  char targetTempChar[6];
  char topBuffer[32];
  char bottomBuffer[32];
  bool selectTemp = true;
  bool runMotor = true;
  //byte rpm = MOTOR_RPM;
  byte rpm = MOTOR_START_PCT;
  byte directionInterval = 5;
  
  while(selectTemp)
  {
    controlTemp();
    if(millis() - previousMillis >= SECONDS_MS * TEMP_UPDATE_INTERVAL || button)
    {
      currentTemperature = collectTemperatures();
      Serial.print("Current Temperature: ");
      Serial.println(currentTemperature);
      Serial.print("Target Temperature: ");
      Serial.println(targetTemperature);
      Serial.print("PID Output: ");
      Serial.println(pidOutput);
      dtostrf(currentTemperature, 5, 2, currentTempChar);
      dtostrf(targetTemperature, 5, 2, targetTempChar);
      sprintf(bottomBuffer, "%sC / %sC", currentTempChar, targetTempChar);
      drawDisplay("Set Temp", bottomBuffer);
      previousMillis = millis();
    }
    button = lcd.readButtons();
    if(button & BUTTON_LEFT)
      return;
    if(button & BUTTON_UP)
      targetTemperature += TEMP_ADJUSTMENT_PRECISION;
    if(button & BUTTON_DOWN)
      targetTemperature -= TEMP_ADJUSTMENT_PRECISION;
    if(button & BUTTON_SELECT)
      selectTemp = false;
  }
  
  // Rotary Part
  while(true)
  {
    controlTemp();
    if(runMotor)
      controlMotor(rpm, directionInterval);
    else
      controlMotor(0, 0);
    if(millis() - previousMillis >= SECONDS_MS * TEMP_UPDATE_INTERVAL || button)
    {
      currentTemperature = collectTemperatures();
      dtostrf(currentTemperature, 5, 2, currentTempChar);
      dtostrf(targetTemperature, 5, 2, targetTempChar);
      sprintf(topBuffer, "%sC / %sC", currentTempChar, targetTempChar);
      if(runMotor)
      {
        sprintf(bottomBuffer, "%02d%% : I %02d", rpm, directionInterval);
        drawDisplay(topBuffer, bottomBuffer); 
      }
      else
      {
        drawDisplay(topBuffer, "Paused"); 
      }
      previousMillis = millis();
    }
    button = lcd.readButtons();
    if(button)
    {
      switch(button)
      {
        case BUTTON_UP:
        {
          // if(rpm < MOTOR_RPM)
          if(rpm < 100)
            ++rpm;
          button = 0;
          break;
        }
        case BUTTON_DOWN:
        {
          // if(rpm > MOTOR_MIN_RPM)
          if(rpm > MOTOR_MIN_PCT)
            --rpm;
          button = 0;
          break;
        }
        case BUTTON_LEFT:
        {
          if(directionInterval > 1)
            --directionInterval;
          button = 0;
          break;
        }
        case BUTTON_RIGHT:
        {
          if(directionInterval < 255)
            ++directionInterval;
          button = 0;
          break;
        }
        case BUTTON_SELECT:
        {
          if(runMotor)
          {
            Serial.println("Pausing Motor");
            drawDisplay(topBuffer, "Paused");
            previousMotorMillis = 0;
            previousCoastMillis = 0;
            runMotor = false;
          }
          else
          {
            Serial.println("Running Motor");
            previousMotorMillis = 0;
            previousCoastMillis = 0;
            runMotor = true;
          }
          break;
        }
      }
      controlMotor(rpm, directionInterval);
      sprintf(topBuffer, "%sC / %sC", currentTempChar, targetTempChar);
      sprintf(bottomBuffer, "%02d%% : I %02d", rpm, directionInterval);
      drawDisplay(topBuffer, bottomBuffer); 
    }
  }
}
