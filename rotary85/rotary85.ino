 /* DevDawg Rotary
 *  
 * Open Source Arduino-Based Rotary Film Development Controller.
 * by Tim Soderstrom
 *  
 *  Licensed under the GPLv3 (see LICENSE for more information)
 *  
 *  This is a smaller program that just handles turning the rotary
 *  (so no display output, no temps, etc.)
 *  Effectively it's a fancy 555 style timer circuit :P
 */

/**********
 * Defines 
 **********/
#define SECONDS_MS 1000

#define FORWARD 0
#define REVERSE 1

#define ON 1
#define OFF 0

// Specified Motor RPM
// #define MOTOR_RPM 76
// #define MOTOR_RPM_LOW 45
// Motor Start Percentage
#define MOTOR_START_PCT 100

// Note the DC motors used are linear when it comes to percentages
// e.g. 50% doesn't seem to be half of the rotations.
#define MOTOR_SPEED_LOW_PCT 25
#define MOTOR_SPEED_HIGH_PCT 100

// How long to free spin motor when changing direction
#define COAST_MOTOR_MILLIS 1000

// How many seconds between direction changes
#define DEFAULT_DIRECTION_INTERVAL 10

// How many MS to wait for debounce
#define DEBOUNCE_DELAY 50

/***********
 * Includes 
 ***********/

/*******
 * Pins
 *******/
/* For ATTiny85 */
#define MOTOR_SPEED 0
#define MOTOR_X A2
#define MOTOR_Y A3
//#define SPEED_BUTTON 1
//#define RUN_BUTTON 2

#define SPEED_BUTTON 1
#define RUN_BUTTON 2


/************
 * Variables 
 ************/
unsigned long previousMotorMillis = 0;
unsigned long previousCoastMillis = 0;

bool motorDirection = FORWARD;
// If we need to coast
bool motorCoast = false;
// If we are actively coasting
bool motorCoasting = false;
// Whether or not to run the motor
// Based on switch
bool motorRun = false;

// Motor Speed
byte motorSpeedPct = 100;

// Debounce
int lastButtonState = LOW;
int buttonState = LOW;
unsigned long lastDebounceTime = 0;


/*******
 * Code 
 ********/

void setup()
{
  pinMode(MOTOR_SPEED, OUTPUT);
  pinMode(MOTOR_X, OUTPUT);
  pinMode(MOTOR_Y, OUTPUT);
 
  pinMode(RUN_BUTTON, INPUT_PULLUP);
  pinMode(SPEED_BUTTON, INPUT_PULLUP);

  /* Start forward at start speed */
  analogWrite(MOTOR_SPEED, toSpeed(MOTOR_SPEED_HIGH_PCT));
  digitalWrite(MOTOR_X, HIGH);
  digitalWrite(MOTOR_Y, LOW);
}

void loop()
{
  runMotor();
}

void runMotor()
{
  byte directionInterval = DEFAULT_DIRECTION_INTERVAL;
  bool runMotor = true;
  bool runButtonHeld = false;
  bool speedButtonHeld = false;
  
  while(true)
  {
    // Checks if a button was pressed (and held)
    // to know if we need to change the state.
    // Note the buttonHeld bool is to avoid 
    // us from flipping states rapidly if the button
    // is just being held down.
    if(checkButton(RUN_BUTTON) == LOW)
    {
      if(!runButtonHeld)
      {
        if(runMotor)
          runMotor = false;
        else
        {
          runMotor = true;
          motorCoasting = false;
          motorCoast = false;
        }
          
      }
      runButtonHeld = true;
    }
    else
    {
      runButtonHeld = false;
    }
    
    // Read speed button with no bounce (fix me later)
    if(digitalRead(SPEED_BUTTON) == LOW)
    {
      if(motorSpeedPct == MOTOR_SPEED_LOW_PCT)
        motorSpeedPct = MOTOR_SPEED_HIGH_PCT;
      else
        motorSpeedPct = MOTOR_SPEED_LOW_PCT;
    }
 
    if(runMotor)
      controlMotor(motorSpeedPct, directionInterval);
    else
      controlMotor(0, 0);
  }
}


int checkButton(byte button)
{
  int reading = digitalRead(button);
  // If we saw a change since the last time, we now need to wait for debounce
  if (reading != lastButtonState)
    lastDebounceTime = millis();
  if (millis() - lastDebounceTime > DEBOUNCE_DELAY)
  {
    // If the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;
    }
  }
  lastButtonState = reading;
  return buttonState;
}

void controlMotor(byte speed, byte directionInterval)
{
  // Is speed is set to zero, just stop the motor and return
  // No need to worry about directions or coasting.
  if(speed == 0)
  {
    analogWrite(MOTOR_SPEED, 0);
    // We set these because if speed becomes non-zero, we 
    // want to make sure we start immediately and not coast.
    
    previousMotorMillis = millis();
    previousCoastMillis = millis();
    return;
  }
    
  if(directionInterval > 0)
  {
    // Coast on direction change
    if(motorCoast)
    {
      analogWrite(MOTOR_SPEED, 0);
      motorCoasting = true;
      if (millis() - previousCoastMillis >= COAST_MOTOR_MILLIS && motorCoast)
      {
        motorCoasting = false;
      }
    }
    // If we're not coasting, make sure we set the speed to whatever is being requested
    else if (!motorCoast)
      analogWrite(MOTOR_SPEED, toSpeed(speed));
    if(millis() - previousMotorMillis >= SECONDS_MS * directionInterval)
    { 
      // Flag we need to coast the motor
      // If we are coasting, do nothing
      if(motorCoasting)
      {
        
      }
      // If we are not currently coasting, but 
      else if(!motorCoast)
      {
        previousCoastMillis = millis();
        motorCoast = true;
      }
      // Now Change Direction
      else if(!motorCoasting)
      {
        analogWrite(MOTOR_SPEED, toSpeed(speed));
        changeDirection();
        motorCoast = false;
        previousMotorMillis = millis();
      }
    }
  }
  /* If directionInterval is zero, never change speed */
  else
  {
    analogWrite(MOTOR_SPEED, toSpeed(speed));
    digitalWrite(MOTOR_X, HIGH);
    digitalWrite(MOTOR_Y, LOW);
    motorDirection = FORWARD;
  }
}

int toSpeed(byte value)
{
  // Based on RPM values
  // return map(value, 0, MOTOR_RPM, 0, 255);
  // Based on percentages
  return map(value, 0, 100, 0, 255);
}

void changeDirection()
{
  if(motorDirection == FORWARD)
  {
    digitalWrite(MOTOR_X, LOW);
    digitalWrite(MOTOR_Y, HIGH);
    motorDirection = REVERSE;
  }
  else
  {
    digitalWrite(MOTOR_X, HIGH);
    digitalWrite(MOTOR_Y, LOW);
    motorDirection = FORWARD;
  }
}
