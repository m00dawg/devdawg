 /* DevDawg Rotary45
 *  
 * Open Source Arduino-Based Rotary Film Controller.
 * by Tim Soderstrom
 *  
 *  Licensed under the GPLv3 (see LICENSE for more information)
 *  
 *  This is a smaller program that just handles turning the rotary
 *  (so no display output, no temps, etc.)
 *
 *  This one is the simpler variant designed to run on ATTiny45's.
 *  It replaces AccelStepper with the standard Stepper library with the
 *  only trade-off being the acceleration on direction changes aren't as smooth.
 *  
 *  If you can find an ATTiny85, use the other sketch. At the time of this writing,
 *  45's were much more plentiful so I opted to write a version that could fit on one of them.
 */

/**********
 * Defines 
 **********/

#define SECONDS_MS 1000
#define SECONDS_PER_MINUTE 60

#define FORWARD 1
#define REVERSE -1

#define ON 1
#define OFF 0

/***********
 * Includes 
 ***********/
#include <Stepper.h>

/*******
 * Pins
 *******/
/* For ATTiny85 */
/*
#define MOTOR_1A A1
#define MOTOR_2A A2
#define MOTOR_3A A3
#define MOTOR_4A 1
#define BUTTON 0
*/
#define MOTOR_1A 0
#define MOTOR_2A 1
#define MOTOR_3A 2
#define MOTOR_4A 3
#define BUTTON 4


/***********
 * Constants
 ***********/
const int stepsPerRevolution = 200;               // The steps per revolution of the stepper
const int rpm = 45;                               // The target RPM
const int directionInvervalMS = 10 * SECONDS_MS;  // How many milliseconds before we change direction
const int directionDelayMS = 100;                 // How many milliseconds to pause for on direction change
const int speedRampDelay = 10;                     // How many milliseconds to delay before adding to our speed for ramp
const int speedRampAdd = 5;                       // How many steps to add to the speed when we are ramping up
const int buttonDebounceDelayMS = 250;            // How many milliseconds to pause for debounce (this is blocking)

/************
 * Variables 
 ************/
unsigned long directionMillis;
unsigned long speedMillis;
bool motorState = OFF;
int motorDirection = FORWARD;
int currentSpeed = 0;

/*********
 * Objects
 *********/
Stepper stepper(stepsPerRevolution, MOTOR_1A, MOTOR_2A, MOTOR_3A, MOTOR_4A);

 
//AccelStepper stepper(AccelStepper::FULL4WIRE, MOTOR_1A, MOTOR_2A, MOTOR_3A, MOTOR_4A);



/*******
 * Code 
 ********/

void setup() {
  // Initialize the Button
  pinMode(BUTTON, INPUT_PULLUP);

  //stepper.setSpeed(0);
  //directionMillis = millis();
}

void loop() {
  /* Check for button press */
  if(digitalRead(BUTTON) == LOW) {
    delay(buttonDebounceDelayMS);
    if(motorState == ON) {
      motorState = OFF;
    }
    else {
      motorState = ON;
      /* Reset the direction value to prevent rotating less than the dsired interval */
      directionMillis = millis();
    } 
  }

  /* Motor is running */
  if(motorState == ON)
  {
    /* Change direction every interval seconds, pausing for a bit in between */
    if(directionMillis + directionInvervalMS <= millis()) {
      stepper.setSpeed(0);
      delay(directionDelayMS);
      currentSpeed = 0;
      if(motorDirection == FORWARD) {      
        motorDirection = REVERSE;
      }
      else {
        motorDirection = FORWARD;
      }
      directionMillis = millis();
    }
  
    /* On direction change, we ramp the speed up instead of trying to go at full speed right after a direction change */
    if(currentSpeed < rpm && speedMillis + speedRampDelay <= millis())
    {
      currentSpeed += speedRampAdd;
      stepper.setSpeed(currentSpeed);
      speedMillis = millis();
    }
  
    /* Run the motor */
    if(motorDirection == FORWARD) {
      stepper.step(1);
      //stepper.setSpeed(currentSpeed);    
    }
    else {
      stepper.step(-1);
      //stepper.setSpeed(currentSpeed * -1);
    }
  }
  /* Motor is disabled */
  else {
    currentSpeed = 0;
    stepper.setSpeed(0);
  }
}
