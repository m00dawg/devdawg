 /* DevDawg Rotary85
 *  
 * Open Source Arduino-Based Rotary Film Controller.
 * by Tim Soderstrom
 *  
 *  Licensed under the GPLv3 (see LICENSE for more information)
 *  
 *  This is a smaller program that just handles turning the rotary
 *  (so no display output, no temps, etc.)
 *
 *  It requires an ATTiny85 due to the use of AccelStepper though this
 *  allows for nice smooth ramping of the motor after it changes
 *  direction.
 *  
 *  Since ATTiny85's are hard to source, see the other sketch in this
 *  repository for a simpler program that can fit on an ATTiny45.
 */

/**********
 * Defines 
 **********/

#define SECONDS_MS 1000
#define SECONDS_PER_MINUTE 60

#define FORWARD 1
#define REVERSE -1

#define ON 1
#define OFF 0

/***********
 * Includes 
 ***********/
#include <AccelStepper.h>

/*******
 * Pins
 *******/
/* For ATTiny85 */
/*
#define MOTOR_1A A1
#define MOTOR_2A A2
#define MOTOR_3A A3
#define MOTOR_4A 1
#define BUTTON 0
*/
#define MOTOR_1A 0
#define MOTOR_2A 1
#define MOTOR_3A 2
#define MOTOR_4A 3
#define BUTTON 4


/***********
 * Constants
 ***********/
const int stepsPerRevolution = 200;               // The steps per revolution of the stepper
const int rpm = 45;                               // The target RPM
const int directionInvervalMS = 10 * SECONDS_MS;  // How many milliseconds before we change direction
const int directionDelayMS = 100;                 // How many milliseconds to pause for on direction change
const int speedRampDelay = 1;                     // How many milliseconds to delay before adding to our speed for ramp
const int speedRampAdd = 5;                       // How many steps to add to the speed when we are ramping up
const int buttonDebounceDelayMS = 250;            // How many milliseconds to pause for debounce (this is blocking)

/************
 * Variables 
 ************/
unsigned long directionMillis;
unsigned long speedMillis;
bool motorState;
int motorDirection;
int currentSpeed;
int stepsPerSecond;

/*********
 * Objects
 *********/
AccelStepper stepper(AccelStepper::FULL4WIRE, MOTOR_1A, MOTOR_2A, MOTOR_3A, MOTOR_4A);



/*******
 * Code 
 ********/

void setup() {
  // Initialize the Button
  pinMode(BUTTON, INPUT_PULLUP);

  // Calculate the steps per second based on the RPM and steps per revolution
  stepsPerSecond = (rpm * stepsPerRevolution) / SECONDS_PER_MINUTE;

  motorDirection = FORWARD;
  currentSpeed = 0;
  stepper.setMaxSpeed(stepsPerSecond);
  stepper.setSpeed(0);
  directionMillis = millis();
}

void loop() {
  /* Check for button press */
  if(digitalRead(BUTTON) == LOW) {
    delay(buttonDebounceDelayMS);
    if(motorState == ON) {
      motorState = OFF;
    }
    else {
      motorState = ON;
      /* Reset the direction value to prevent rotating less than the dsired interval */
      directionMillis = millis();
    } 
  }

  /* Motor is running */
  if(motorState == ON)
  {
    /* Change direction every interval seconds, pausing for a bit in between */
    if(directionMillis + directionInvervalMS <= millis()) {
      stepper.stop();
      delay(directionDelayMS);
      currentSpeed = 0;
      if(motorDirection == FORWARD) {      
        motorDirection = REVERSE;
      }
      else {
        motorDirection = FORWARD;
      }
      directionMillis = millis();
    }
  
    /* On direction change, we ramp the speed up instead of trying to go at full speed right after a direction change */
    if(currentSpeed < stepsPerSecond && speedMillis + speedRampDelay <= millis())
    {
      currentSpeed += speedRampAdd;
      speedMillis = millis();
    }
  
    /* Assign the current speed to the motor, negative values mean spin opposite way */
    if(motorDirection == FORWARD) {
      stepper.setSpeed(currentSpeed);    
    }
    else {
      stepper.setSpeed(currentSpeed * -1);
    }
  
    /* Run the motor */
    stepper.runSpeed();
  }
  /* Motor is disabled */
  else {
    //stepper.stop();
    currentSpeed = 0;
    stepper.disableOutputs();
  }
}
