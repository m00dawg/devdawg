   /* DevDawg Rotary Feather M0 Express
 *  
 * Open Source Arduino-Based Rotary Film Controller.
 * by Tim Soderstrom
 *  
 *  Licensed under the GPLv3 (see LICENSE for more information)
 *
 * This is the Adafruit Feather M0 Version which uses the
 * Feather motor and Feather display stackable boards.
 * 
 * Boards:
 * https://www.adafruit.com/product/2927
 * https://www.adafruit.com/product/2900
 * https://www.adafruit.com/product/3403
 */

/**********
 * Defines 
 **********/
#define SECONDS_MS 1000
#define SECONDS_PER_MINUTE 60

#define FORWARD 1
#define REVERSE -1

#define ON 1
#define OFF 0


// OLED FeatherWing buttons map to different pins depending on board.
// The I2C (Wire) bus may also be different.
#if defined(ESP8266)
  #define BUTTON_A  0
  #define BUTTON_B 16
  #define BUTTON_C  2
  #define WIRE Wire
#elif defined(ESP32)
  #define BUTTON_A 15
  #define BUTTON_B 32
  #define BUTTON_C 14
  #define WIRE Wire
#elif defined(ARDUINO_STM32_FEATHER)
  #define BUTTON_A PA15
  #define BUTTON_B PC7
  #define BUTTON_C PC5
  #define WIRE Wire
#elif defined(TEENSYDUINO)
  #define BUTTON_A  4
  #define BUTTON_B  3
  #define BUTTON_C  8
  #define WIRE Wire
#elif defined(ARDUINO_FEATHER52832)
  #define BUTTON_A 31
  #define BUTTON_B 30
  #define BUTTON_C 27
  #define WIRE Wire
#elif defined(ARDUINO_ADAFRUIT_FEATHER_RP2040)
  #define BUTTON_A  9
  #define BUTTON_B  8
  #define BUTTON_C  7
  #define WIRE Wire1
#else // 32u4, M0, M4, nrf52840 and 328p
  #define BUTTON_A  9
  #define BUTTON_B  6
  #define BUTTON_C  5
  #define WIRE Wire
#endif

/*******
 * Pins
 *******/

/**********
 * Dotstar
 **********/
#define NUMPIXELS  1
#define DATAPIN    7
#define CLOCKPIN   8
#define POWER_LED 13

/***********
 * Includes 
 ***********/
//#include <Stepper.h>
#include <AccelStepper.h>
#include <Adafruit_DotStar.h> // This is just here to turn off the damn on board LED
#include <Adafruit_MotorShield.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>


/***********
 * Constants
 ***********/
const int stepsPerRevolution = 200;               // The steps per revolution of the stepper
const int rpm = 45;                               // The target RPM
const int directionInvervalMS = 10 * SECONDS_MS;  // How many milliseconds before we change direction
const int directionDelayMS = 100;                 // How many milliseconds to pause for on direction change
const int speedRampAdd = 3;                       // How many steps to add to the speed when we are ramping up
const int buttonDebounceDelayMS = 250;            // How many milliseconds to pause for debounce (this is blocking)
const int motorPort = 1;                          // Which port the motor is connected to

/************
 * Variables 
 ************/
unsigned long directionMillis;
unsigned long speedMillis;
bool motorState;
int motorDirection;
int currentSpeed;
int stepsPerSecond;

/*********
 * Objects
 *********/
//Stepper stepper(stepsPerRevolution, MOTOR_1A, MOTOR_2A, MOTOR_3A, MOTOR_4A);
Adafruit_DotStar strip(NUMPIXELS, DATAPIN, CLOCKPIN, DOTSTAR_BRG);
Adafruit_MotorShield AFMS;
Adafruit_StepperMotor motor;

/*******
 * Code 
 ********/

// Set up the stepper. Unlike controlling a h-bridge, this doesn't
// require constantly calling steps and is async so we can do other 
// stuff (like drive a display)
// For more info: https://learn.adafruit.com/adafruit-stepper-dc-motor-featherwing/using-stepper-motors
void step_forward() {  
  motor.onestep(FORWARD, DOUBLE);
}
void step_backward() {  
  motor.onestep(BACKWARD, DOUBLE);
}
AccelStepper stepper(step_forward, step_backward);


void setup() {
  // Turn off that incredibly bright onboard LED
  pinMode(POWER_LED, OUTPUT);
  digitalWrite(POWER_LED, LOW); 

  // Turn off the other incredibly bright LED
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  
  // Initialize the Buttons
  // BUTTON_C = start/stop
  pinMode(BUTTON_A, INPUT_PULLUP);
  pinMode(BUTTON_B, INPUT_PULLUP);
  pinMode(BUTTON_C, INPUT_PULLUP);

  // text display tests
  display.setTextSize(1);
  display.setTextColor(SSD1306_WHITE);
  display.setCursor(0,0);
  display.print("RPM: ");
  display.println("IP: 10.0.1.23");
  display.println("Sending val #0");
  display.setCursor(0,0);
  display.display(); // actually display all of the above

  //Adafruit_StepperMotor *myMotor = AFMS.getStepper(stepsPerRevolution, motorPort);
}

void loop() {
  /* Check for button presses */
  if(digitalRead(BUTTON_A) == LOW) {
    delay(buttonDebounceDelayMS);
    rpm++;
  }
  if(digitalRead(BUTTON_B) == LOW) {
    delay(buttonDebounceDelayMS);
    rpm--;
  }  
  if(digitalRead(BUTTON_C) == LOW) {
    delay(buttonDebounceDelayMS);
    if(motorState == ON) {
      motorState = OFF;
    }
    else {
      motorState = ON;
      /* Reset the direction value to prevent rotating less than the dsired interval */
      directionMillis = millis();
    } 
  }

  /* Motor is running */
  if(motorState == ON)
  {
    /* Change direction every interval seconds, pausing for a bit in between */
    if(directionMillis + directionInvervalMS <= millis()) {
      stepper.setSpeed(0);
      delay(directionDelayMS);
      currentSpeed = 0;
      if(motorDirection == FORWARD) {      
        motorDirection = REVERSE;
      }
      else {
        motorDirection = FORWARD;
      }
      directionMillis = millis();
    }
  
    if(currentSpeed < rpm && speedMillis <= millis())
    {
      currentSpeed += speedRampAdd;
      stepper.setSpeed(currentSpeed);
      speedMillis = millis();
    }
  
    /* Run the motor */
    if(motorDirection == FORWARD) {
      stepper.setSpeed(currentSpeed);    
    }
    else {
      stepper.setSpeed(currentSpeed * -1);
    }
  }
  /* Motor is disabled */
  else {
    currentSpeed = 0;
    stepper.setSpeed(0);
  }
}
